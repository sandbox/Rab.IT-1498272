/**
 * Created by Quantum Web.
 * User: rabit
 * Date: 4/14/12
 * Time: 4:38 PM
 */

jQuery(document).ready(function(){

  var txtAreaId = jQuery (".text-full.form-textarea").attr ('id');

  var optionHtmlForTxtArea = ''
    + 'Press <b>ctrl + m</b> to toggle Bangla phonetic input';
  // console.log ('test alert');
  jQuery('#' + txtAreaId).before ('<span>'+optionHtmlForTxtArea+'</span>');

  jQuery('#'+txtAreaId).avro({'bangla':false});

});